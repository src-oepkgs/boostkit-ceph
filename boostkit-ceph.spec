%define file0 boostkit-ceph
%define file2 partition.sh
%define file3 create_osd.sh
%define file4 python.zip

%define file10 ceph-boost
%define file11 ceph-boost.service

%define file20 KAEzlib/KAEdriver.zip
%define file21 KAEzlib/KAE.zip
%define file22 KAEzlib/KAEzip.zip
%define file23 KAEzlib/deploy_kae.sh

%define file30 cron_sdslog
%define file31 logrotate_sdslog.conf
%define file32 hi_coreutil.h
%define file33 kps_bluestore.h
%define file34 sdslog.h
%define file35 sdslog_param.h
%define file36 libkps_bluestore.so
%define file37 libkps_ec.so
%define file38 libsdslog.so
%define file39 libkps_bluestore.so.1.0.0
%define file40 libkps_ec.so.1.2.1
%define file41 libsdslog.so.1.2.1

%define file50 libglz.so
%define file51 glz.h

%define file60 bcache.tar.gz
%define file61 bcache.h
%define file62 swc.sh
%define file63 kps-swc
%define file64 kps-swc.service

%define file70 libkps_compaction.so
%define file71 kps_compaction.h

%define file80 libksal.so
%define file81 ksal_crc.h
%define file82 ksal_erasure_code.h
%define file83 libdas.so
%define file84 libksal_libc.so
%define file85 das.h
%define file86 ksal_crc.h

%define root_dir /opt/boostkit-ceph/

Name:		boostkit-ceph
Version:	1.7
Release:	1
Summary:	A software to improve performace of Ceph
License:	Commercial
URL:		http://support.huawei.com/enterprise
Source0:	%{file0}
Source2:	%{file2}
Source3:        %{file3}
Source4:	%{file4}

Source10:        %{file10}
Source11:        %{file11}

Source20:	%{file20}
Source21:        %{file21}
Source22:        %{file22}
Source23:        %{file23}

Source30:        %{file30}
Source31:        %{file31}
Source32:        %{file32}
Source33:        %{file33}
Source34:        %{file34}
Source35:        %{file35}
Source36:        %{file36}
Source37:        %{file37}
Source38:        %{file38}
Source39:        %{file39}
Source40:        %{file40}
Source41:        %{file41}

Source50:       %{file50}
Source51:	%{file51}

Source60:	%{file60}
Source61:       %{file61}
Source62:       %{file62}
Source63:       %{file63}
Source64:       %{file64}

Source70:       %{file70}
Source71:       %{file71}

Source80:       %{file80}
Source81:       %{file81}
Source82:       %{file82}
Source83:       %{file83}
Source84:       %{file84}
Source85:       %{file85}
Source86:       %{file86}

Requires:       boostkit-ceph-iopass, boostkit-ceph-KAEzlib, boostkit-ceph-ecturbo, boostkit-ceph-glz, boostkit-ceph-swc, boostkit-ceph-data-compaction, boostkit-ceph-ksal
ExclusiveArch:  aarch64

%description
boostkit-ceph is a software to improve the performance of Ceph.

%global debug_package %{nil}

%define deploydir %{_builddir}/deploy_tools/

%install
mkdir -p %{buildroot}%{root_dir}
%{__install} -Dm755 %{SOURCE0} %{buildroot}%{_bindir}/%{file0}
%{__install} -Dm755 %{SOURCE2} %{buildroot}/etc/ceph/%{file2}
%{__install} -Dm755 %{SOURCE3} %{buildroot}/etc/ceph/%{file3}
unzip -d %{buildroot}%{root_dir} %{SOURCE4}

%{__install} -Dm755 %{SOURCE10} %{buildroot}%{_bindir}/%{file10}
%{__install} -Dm755 %{SOURCE11} %{buildroot}%{_unitdir}/%{file11}

%{__install} -Dm755 %{SOURCE23} %{buildroot}%{root_dir}%{file23}
unzip -d %{buildroot}%{root_dir} %{SOURCE20}
unzip -d %{buildroot}%{root_dir} %{SOURCE21}
unzip -d %{buildroot}%{root_dir} %{SOURCE22}

%{__install} -Dm600 %{SOURCE30} %{buildroot}%{_sysconfdir}/cron.d/%{file30}
%{__install} -Dm600 %{SOURCE31} %{buildroot}%{_sysconfdir}/%{file31}
%{__install} -Dm644 %{SOURCE32} %{buildroot}%{_includedir}/%{file32}
%{__install} -Dm644 %{SOURCE33} %{buildroot}%{_includedir}/%{file33}
%{__install} -Dm644 %{SOURCE34} %{buildroot}%{_includedir}/%{file34}
%{__install} -Dm644 %{SOURCE35} %{buildroot}%{_includedir}/%{file35}
%{__install} -Dm755 %{SOURCE36} %{buildroot}%{_libdir}/%{file36}
%{__install} -Dm755 %{SOURCE37} %{buildroot}%{_libdir}/%{file37}
%{__install} -Dm755 %{SOURCE38} %{buildroot}%{_libdir}/%{file38}
%{__install} -Dm755 %{SOURCE39} %{buildroot}%{_libdir}/%{file39}
%{__install} -Dm755 %{SOURCE40} %{buildroot}%{_libdir}/%{file40}
%{__install} -Dm755 %{SOURCE41} %{buildroot}%{_libdir}/%{file41}

%{__install} -Dm755 %{SOURCE50} %{buildroot}%{_libdir}/%{file50}
%{__install} -Dm644 %{SOURCE51} %{buildroot}%{_includedir}/%{file51}

tar -zxf %{SOURCE60} -C %{buildroot}%{root_dir}
%{__install} -Dm755 %{SOURCE61} %{buildroot}%{root_dir}/%{file61}
%{__install} -Dm755 %{SOURCE62} %{buildroot}%{root_dir}/%{file62}
%{__install} -Dm755 %{SOURCE63} %{buildroot}%{_bindir}/%{file63}
%{__install} -Dm755 %{SOURCE64} %{buildroot}%{_unitdir}/%{file64}

%{__install} -Dm755 %{SOURCE70} %{buildroot}%{_libdir}/%{file70}
%{__install} -Dm644 %{SOURCE71} %{buildroot}%{_includedir}/%{file71}

%{__install} -Dm755 %{SOURCE80} %{buildroot}%{_libdir}/%{file80}
%{__install} -Dm644 %{SOURCE81} %{buildroot}%{_includedir}/ksal/%{file81}
%{__install} -Dm644 %{SOURCE82} %{buildroot}%{_includedir}/ksal/%{file82}
%{__install} -Dm755 %{SOURCE80} %{buildroot}%{_libdir}/%{file83}
%{__install} -Dm755 %{SOURCE80} %{buildroot}%{_libdir}/%{file84}
%{__install} -Dm644 %{SOURCE81} %{buildroot}%{_includedir}/ksal/%{file85}
%{__install} -Dm644 %{SOURCE82} %{buildroot}%{_includedir}/ksal/%{file86}

%clean
%{__rm} -rf %{buildroot}

%files

%package tool
Summary:	deploy tools of boostkit-ceph
Requires:	systemd, wget, openssh, openssh-clients, python2-pip
Requires:	ceph = 2:14.2.8-3
%description tool
deploy tools of boostkit-ceph

%pre tool
rm -rf %{root_dir}

%files tool
%{_bindir}/%{file0}
/etc/ceph/%{file2}
/etc/ceph/%{file3}
%{root_dir}python

%package iopass
Summary:	I/O passthrough tool is used to optimize the I/O process of OSD storage
Requires:	lvm2, systemd, gawk, boostkit-ceph-tool
%description iopass
I/O passthrough tool is used to optimize the I/O process of OSD storage

%files iopass
%{_bindir}/%{file10}
%{_unitdir}/%{file11}

%package KAEzlib
Summary:	KAEzlib optimizes compression and maximizes the CPU capability of procesing OSD processes.
Requires:	libtool, libtool-devel, coreutils, autoconf, automake, m4, kmod, openssl, openssl-devel, kernel-devel, zlib, zlib-devel, make, patch, boostkit-ceph-tool
%description KAEzlib
KAEzlib optimizes compression and maximizes the CPU capability of procesing OSD processes.

%files KAEzlib
%{root_dir}KAEdriver
%{root_dir}KAE
%{root_dir}KAEzip
%{root_dir}%{file23}

%package ecturbo
Summary:        EC Turbo improves the I/O performance of data within a stripe
Requires:	boostkit-ceph-tool

%description ecturbo
EC Turbo improves the I/O performance of data within a stripe

%files ecturbo
%{_sysconfdir}/cron.d/%{file30}
%{_sysconfdir}/%{file31}
%{_includedir}/%{file32}
%{_includedir}/%{file33}
%{_includedir}/%{file34}
%{_includedir}/%{file35}
%{_libdir}/%{file36}
%{_libdir}/%{file37}
%{_libdir}/%{file38}
%{_libdir}/%{file39}
%{_libdir}/%{file40}
%{_libdir}/%{file41}
%{_libdir}/libkps_bluestore.so.1
%{_libdir}/libkps_ec.so.1
%{_libdir}/libsdslog.so.1

%package glz
Summary:        glz has higher compression ratio and better performance
Requires:       boostkit-ceph-tool

%description glz
glz has higher compression ratio and better performance

%files glz
%{_libdir}/%{file50}
%{_includedir}/%{file51}

%package swc
Summary:        swc improves bcache performance through I/O passthrough, Qos policy control and GC policy control.
Requires:	bcache-tools, kmod, dracut, boostkit-ceph-tool
%description swc
swc improves bcache performance through I/O passthrough, Qos policy control and GC policy contro
l.

%files swc
%{root_dir}bcache
%{root_dir}%{file61}
%{root_dir}%{file62}
%{_bindir}/%{file63}
%{_unitdir}/%{file64}

%package data-compaction
Summary:       The data compaction algorithm of the Kunpeng BoostKit for SDS is deployed on the open-source distributed storage cluster Ceph tp eliminate data waste caused by zero padding after compression. The data compaction algorighm improves the data reduction ratio and overall system IOPS, which reduces costs and improves performence.
Requires:	boostkit-ceph-tool
%description data-compaction
The data compaction algorithm of the Kunpeng BoostKit for SDS is deployed on the open-source distributed storage cluster Ceph tp eliminate data waste caused
by zero padding after compression. The data compaction algorighm improves the data reduction ratio and overall system IOPS, which reduces costs and improves performence.

%files data-compaction 
%{_libdir}/%{file70}
%{_includedir}/%{file71}

%package ksal
Summary: Kunpeng Storage Acceleration Library
%description ksal
Kunpeng Storage Acceleration Library
%global debug_package %{nil}

%files ksal
%{_libdir}/%{file80}
%{_includedir}/ksal
%{_includedir}/ksal/%{file81}
%{_includedir}/ksal/%{file82}
%{_libdir}/%{file83}
%{_libdir}/%{file84}
%{_includedir}/ksal/%{file85}
%{_includedir}/ksal/%{file86}

%changelog
* Wed Jan 24 2024 huangbingkang <huangbingkang4@huawei.com>  - 1.7-1
- add ksal
* Tue May 16 2023 luosizhe <luosizhe1@huawei.com> - 1.6-1
- add data compaction
* Tue May 9 2023 huangbingkang <huangbingkang4@huawei.com> - 1.5-1
- add smart write cache
* Thu May 4 2023 huangbingkang <huangbingkang4@huawei.com> - 1.4-1
- fix deploy script
* Mon Apr 10 2023 lipeng <lipeng379@huawei.com> - 1.3-1
- add EC Turbo
* Thu Apr 06 2023 hezewei <hezewei@huawei.com> - 1.2-1
- add LZ4
* Thu Apr 06 2023 huangbingkang <huangbingkang4@huawei.com> - 1.1-1
- add io pass & KAE zlib
