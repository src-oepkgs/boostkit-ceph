# boostkit-ceph

#### Description
Kunpeng BoostKit distributed storage enablement suite focuses on the key challenges of low performance and high cost of open-source Ceph storage. Features such as global cache, intelligent write cache, open-source Ceph system parameter optimization, KAE MD5 digest algorithm, I/O passthrough, and I/O intelligent prefetch improve system performance. Features such as BoostKit compression algorithm, KAE zlib compression, and EC Turbo reduce storage costs. Give full play to Kunpeng's computing advantages and provide cost-effective storage solutions.

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
