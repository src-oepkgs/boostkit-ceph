#!/bin/bash
root_dir=/opt/boostkit-ceph
if [[ "$1" == "install" ]]; then
	cd ${root_dir}/KAEdriver/kae_driver
	make
	make install

	cd ${root_dir}/KAEdriver/warpdrive
	sh autogen.sh
        ./configure
	make
	make install
	
	modprobe uacce
	modprobe hisi_sec2
	modprobe hisi_hpre
	modprobe hisi_rde
	modprobe hisi_zip

	cd ${root_dir}/KAE
        ./configure
        make clean
        make
	make install
	
	cd ${root_dir}/KAEzip
	sh setup.sh install

elif [[ "$1" == "uninstall" ]]; then
	cd ${root_dir}/KAEdriver/kae_driver
        make uninstall

        cd ${root_dir}/KAEdriver/warpdrive
        make uninstall

        cd ${root_dir}/KAE
        make uninstall

        cd ${root_dir}/KAEzip
        sh setup.sh uninstall

	rmmod hisi_sec2
        rmmod hisi_hpre
        rmmod hisi_rde
        rmmod hisi_zip
	rmmod hisi_qm
	rmmod uacce
fi
