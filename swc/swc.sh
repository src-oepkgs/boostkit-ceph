#!/bin/bash
set -e
cd /opt/boostkit-ceph
mv /usr/src/kernels/$(uname -r)/include/trace/events/bcache.h /usr/src/kernels/$(uname -r)/include/trace/events/bcache.h.bak
cp bcache.h /usr/src/kernels/$(uname -r)/include/trace/events/bcache.h
cd bcache
make -C /lib/modules/$(uname -r)/build M=$(pwd)
mv /usr/src/kernels/$(uname -r)/include/trace/events/bcache.h.bak /usr/src/kernels/$(uname -r)/include/trace/events/bcache.h
cp bcache.ko /lib/modules/$(uname -r)/kernel/drivers/md/bcache
depmod -a
dracut --add-drivers bcache -f /boot/initramfs-$(uname -r).img
modprobe bcache

