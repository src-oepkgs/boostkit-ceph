/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: Define hw_ec_partial API
 */

#ifndef HI_CORE_UTIL_H
#define HI_CORE_UTIL_H

#include <cstdint>
#include <map>
#include <vector>
#include <set>
#include <utility>

#include "sdslog_param.h"

/* *
 * @函数功能: 初始化
 * @参数:   paramFilePath {in}，日志参数文件路径
            osdNum {in}，OSD编号
 * @返回值: 0表示成功，非0表示失败
 */
int HiInit(const std::string &paramFilePath, const std::string &osdNum);

/* *
 * @函数功能: 初始化
 * @参数:   param {in}，参数结构体
 * @返回值: 0表示成功，非0表示失败
 */
int HiInit(const SdslogParam &param);

/* *********************小写部分********************* */
/* *
 * @函数功能: 根据客户端写数据范围的偏移、长度和当前的chunk size，确定部分读范围
 * @参数: offset {in}, 偏移
 * @参数: len {in}, 长度
 * @参数: csize {in}, chunk size
 * @参数: headStart {out}, 对齐后的偏移
 * @参数: headLength {out}, 对齐后的长度
 * @返回值: false or true
 */
bool HiSetWriteSection(const uint64_t offset, const uint64_t len, const uint64_t csize, uint64_t &headStart,
    uint64_t &headLength);

/* *
 * @函数功能: 确定部分写过程中的读范围
 * @参数: writeSet {in}, 写范围
 * @参数: csize {in}, chunk size
 * @参数: toRead {in & out}, 作为输入时是初始的读范围，作为输出时是修改后的读范围
 * @返回值: false or true
 */
bool HiRebuildToread(const std::map<uint64_t, uint64_t> &writeSet, uint64_t csize,
    std::map<uint64_t, uint64_t> &toRead);

/* *********************小读部分********************* */
struct HiEcInfo {
    size_t dataCnt;                   // EC数据块个数
    size_t chunkCnt;                  // EC数据块与校验块个数之和
    const std::vector<int> &cMapping; // EC条带内chunk的顺序映射关系
    uint64_t cSize;                   // EC chunk大小
    uint64_t sWidth;                  // EC stripe长度
    uint64_t uSize;                   // EC chunk_unit大小

    HiEcInfo(size_t k, size_t km, const std::vector<int> &chunkMapping, uint64_t chunkSize, uint64_t stripeWidth,
        uint64_t chunkUnitSize = 4096)
        : dataCnt(k), chunkCnt(km), cMapping(chunkMapping), cSize(chunkSize), sWidth(stripeWidth), uSize(chunkUnitSize)
    {}
};

/* *
 * @函数功能: 根据上层请求读数据的范围，计算涉及到的shard id
 * @参数: rRange {in}, 上层请求读数据的范围，格式为<off, len>
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: out {out}, 计算得到的shard id集合
 * @返回值: 无
 */
void HiGetRelatedShards(const std::pair<uint64_t, uint64_t> &rRange, const HiEcInfo &ecInfo, std::set<int> &out);

/* *
 * @函数功能: 根据上层请求读数据的范围，计算涉及到的每个shard内部的读取范围
 * @参数: rRange {in}, 上层请求读数据的范围，格式为<off, len>
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: wants {out}, 计算得到的每个shard内部的读取范围，key为shard id，val为多个区间
 * @返回值: 无
 */
void HiGetShardsRangeToRead(const std::pair<uint64_t, uint64_t> &rRange, const HiEcInfo &ecInfo,
    std::map<int, std::set<std::pair<uint64_t, uint64_t>>> &wants);

/* *
 * @函数功能: 根据对齐后的待写区间，计算所有需要写入的shard id
 * @参数: toWriteChunkAlign {in}, 按chunk对齐后的待写区间
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: wantToWrite {out}, 写入涉及到的所有shard id
 * @返回值: 无
 */
void HiGetWriteToShards(const std::map<uint64_t, uint64_t> &toWriteChunkAlign, const HiEcInfo &ecInfo,
    std::set<int> &wantToWrite);

/* *
 * @函数功能: 计算对象重组涉及到的shard
 * @参数: start {in}, 条带上待计算的起始shard id
 * @参数: count {in}, 条带上待计算的shard个数
 * @参数: len {in}, 条带内涉及chunk的长度
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: shardID {out}, 计算得到的shard id集合
 * @返回值: 无
 */
void HiGetReconstructShards(const unsigned int start, const unsigned int count, const uint64_t len,
    const HiEcInfo &ecInfo, std::vector<int> &shardID);

/* *
 * @函数功能: 对于各个shard读回的数据，得到一个二元组向量，其中向量的每个元素的内容为<chunkIndex, dataLen>;
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: rRange {in}, 上层请求读数据的范围，格式为<off, len>
 * @参数: cInfo {out}, 一个二元组向量，其中向量的每个元素为<chunkIndex, dataLen>
 * 分别表示chunk的索引编号和该chunk内部的数据长度
 * @返回值: 无
 */
void HiReconstructPrepare(const HiEcInfo &ecInfo, const std::pair<uint64_t, uint64_t> &rRange,
    std::vector<std::pair<uint64_t, uint64_t>> &cInfo);

/* *
 * @函数功能：计算对象补零之后，各个shard中补零的范围，包括补零offset，补零长度
 * @参数：rRange {in}, 上层请求的写数据的范围，格式为<off, len>;
 * @参数：ecInfo {in}, EC相关配置参数
 * @参数：zeroMap {out}, 每个shard对应的补零范围，格式为：<shardID, <offset, length>>
 * 分别表示shardID，补零的偏移，补零的长度
 * @返回值：无
 */
void HiGetShardZeroRange(const std::pair<uint64_t, uint64_t> &rRange, const HiEcInfo &ecInfo,
    std::map<int, std::pair<uint64_t, uint64_t>> &zeroMap);

/* *
 * @函数功能: 重新加载参数
 * @参数:   paramFilePath {in}，日志参数文件路径
            osdNum {in}，OSD编号
 * @返回值: 0表示成功，非0表示失败
 */
int HiReload(const std::string &paramFilePath, const std::string &osdNum);

/* *
 * @函数功能: 重新加载参数
 * @参数:   param {in}，参数结构体
 * @返回值: 0表示成功，非0表示失败
 */
int HiReload(const SdslogParam &param);

/* *
 * @函数功能: 应用结束
 * @参数: 无
 * @返回值: 无
 */
void HiFinish();

/* *
 * @函数功能: 偏移根据chunk对齐
 * @参数: in {in}，待对齐的数据
 * @参数: csize {in}，chunk size
 * @返回值: 对齐后的数据
 */
std::pair<uint64_t, uint64_t> HiOffsetLenToChunkBounds(std::pair<uint64_t, uint64_t> in, uint64_t csize);

/* *
 * @函数功能: 判断是否update
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: range {in}，数据范围
 * @返回值: 返回true时使用update
 */
bool HiShouldUseUpdate(const HiEcInfo &ecInfo, const std::pair<uint64_t, uint64_t> &range);

struct HiChunkInfo {
    uint64_t index;    // 索引
    uint64_t count;    // 数量
    uint64_t chunkLen; // chunk长度

    HiChunkInfo(uint64_t indx, uint64_t cnt, uint64_t cl) : index(indx), count(cnt), chunkLen(cl) {}
};

/* *
 * @函数功能: 准备update的chunk数据
 * @参数: ecInfo {in}, EC相关配置参数
 * @参数: offset {in}，偏移
 * @参数: blLength {in}，数据bufferlist长度
 * @参数: chunkInfo {out}，chunk信息
 * @返回值: 无
 */
void HiPrepareUpdateChunks(const HiEcInfo &ecInfo, uint64_t offset, uint64_t blLength, HiChunkInfo &chunkInfo);

/* *
 * @函数功能: 使用汇编实现xor计算
 * @参数: src {in}, 源数据
 * @参数: parity {out}，计算后的数据
 * @参数: srcSize {in}，源数据数据块个数
 * @参数: size {in}，源数据中每个数据块的大小
 * @返回值: 无
 */
void HiRegionNeonXor(char **src, char parity[], int srcSize, unsigned size);
#endif
