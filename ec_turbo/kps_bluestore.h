/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * Description: KPS bluestore
 */
#ifndef KPS_BLUESTORE_H
#define KPS_BLUESTORE_H

#include <cstdint>
#include <cstdlib>
#include <vector>

using slot = uint64_t;

using KpsExtent = struct KpsExtentS {
    uint64_t off;
    uint32_t len;
};

using KpsAllocInfo = struct KpsAllocInfoS {
    uint64_t wantSize;
    uint64_t allocUnit;
};

using KpsAllocPos = struct KpsAllocPosS {
    uint64_t l0Pos;
    uint64_t l1Pos;
    uint64_t l2Pos;
};

class KpsAllocator {
public:
    KpsAllocator(uint32_t totalSlotsPerSlotset, uint64_t allocUnit, KpsAllocPos pos,
        size_t& parilL1Count, size_t& unalcL1Count);
    ~KpsAllocator() {}

    uint64_t KpsAllocate(KpsAllocInfo info, std::vector<slot>& l0, std::vector<slot>& l1,
        std::vector<slot>& l2, std::vector<KpsExtent>& extents);

private:

    /* the disk space that every bit in l0 stand for，in bytes */
    uint64_t l0Granularity;
    /* L1 entry count whoes space is partially allocated */
    size_t& partialL1Count;
    /* L1 entry count whoes space is unused */
    size_t& unallocL1Count;

    /* l0's assigned location */
    uint64_t l0Pos;
    /* l1's assigned location */
    uint64_t l1Pos;
    /* l2's assigned location */
    uint64_t l2Pos;

    /* slot count in a slotset */
    uint32_t slotNumPerSlotset;

    /* L1 entry count in a L2 entry */
    uint32_t l1EntriesPerL2Entry;
    /* L0 entry count in a L1 entry */
    uint32_t l0EntriesPerL1Entry;

    void AdjL1SlotValue(std::vector<slot>& l0, std::vector<slot>& l1, uint64_t slotPos, uint64_t l1Pos);
    void AdjL2SlotValue(std::vector<slot>& l1, std::vector<slot>& l2, uint64_t slotPos, uint64_t l2Pos);
    void KpsAllocL0(KpsAllocInfo& info, std::vector<slot>& l0, uint64_t& allocated,
        std::vector<KpsExtent>& extents);
    void KpsAllocL1(KpsAllocInfo& info, std::vector<slot>& l0, std::vector<slot>& l1,
        uint64_t& allocated, std::vector<KpsExtent>& extents);
    uint64_t KpsAllocL2(KpsAllocInfo& info, std::vector<slot>& l0, std::vector<slot>& l1,
        std::vector<slot>& l2, std::vector<KpsExtent>& extents);
};

#endif
