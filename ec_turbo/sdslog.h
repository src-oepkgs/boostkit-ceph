/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: Define sdslog API
 */

#ifndef SDS_LOG_H
#define SDS_LOG_H

#include "sdslog_param.h"

/* ****************************************************************************
 功能描述: 设置参数，启动模块记录日志的功能。该接口可以重复调用，以最近一次有效的参数为准。
           日志文件只允许存储在/var/log和/home路径内。该接口传入参数的路径。

     参数: moduleName ，in 用户模块名，与日志文件名相关。
           paramFilePath ，in 参数文件路径。

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_EMPTY_PARAM   没有传入参数文件路径
           SDSLOG_PARAM_FILE_CANNNOT_ACCESS 参数文件不可用
           SDSLOG_INVALID_PARAM 参数错误
**************************************************************************** */
int SdslogSetAndInit(const std::string &moduleName, const std::string &paramFilePath);

/* ****************************************************************************
 功能描述: 设置参数，启动模块记录日志的功能。该接口可以重复调用，以最近一次有效的参数为准。
           日志文件只允许存储在/var/log和/home路径内。该接口传入参数的结构体。

     参数: param ，in 参数的结构体。

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_EMPTY_PARAM   没有传入参数文件路径
           SDSLOG_PARAM_FILE_CANNNOT_ACCESS 参数文件不可用
           SDSLOG_INVALID_PARAM 参数错误
**************************************************************************** */
int SdslogSetAndInit(SdslogParam &param);


/* ****************************************************************************
 功能描述: 写日志。日志内容可以用可变参数形式传入，类似printf。

     参数: level ，in 当前需要记录日志的等级。
           moduleName ，in 用户模块名。
           subModule ，in 子模块名。
           fmt，in 日志内容

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_INVALID_MODULE   没有初始化此模块
**************************************************************************** */
int SdslogWriteLog(int level, const std::string &moduleName, const std::string &subModule, const char *fmt, ...)
    __attribute__((format(printf, 4, 5)));

/* ****************************************************************************
 功能描述: 将mem_log的内容移动到日志文件中

     参数: moduleName ，in 用户模块名。

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_INVALID_MODULE   没有初始化此模块
**************************************************************************** */
int SdslogMemToFile(const std::string &moduleName);

/* ****************************************************************************
 功能描述: 获取当前生效的参数值

     参数: moduleName ，in 用户模块名。
           param ，out 参数值。

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_INVALID_MODULE   没有初始化此模块
**************************************************************************** */
int SdslogGetCurParam(const std::string &moduleName, SdslogParam &param);

/* ****************************************************************************
 功能描述: 停止记录日志功能。

     参数: moduleName ，in 用户模块名。

   返回值: SDSLOG_SUCCESS    表示成功
           SDSLOG_INVALID_MODULE   没有初始化此模块
**************************************************************************** */
int SdslogStopRecordLog(const std::string &moduleName);

/* ****************************************************************************
 功能描述: 获取日志模块版本号

     参数: ver ，out 日志模块版本号

   返回值: SDSLOG_SUCCESS    表示成功，其他表示失败
**************************************************************************** */
int SdslogGetVersion(std::string &ver);

/* ****************************************************************************
 功能描述: 重新加载模块。mem log移动到文件里，停止记录日志，再初始化。pre_moduleName和moduleName可以相同。

     参数: pre_moduleName ，in reload前的用户模块名
           moduleName ，in 用户模块名。
           fileName ，in 参数文件路径

   返回值: SDSLOG_SUCCESS    表示成功，其他表示失败
**************************************************************************** */
int SdslogReloadFileParam(const std::string &preModuleName, const std::string &moduleName, const std::string &fileName);

/* ****************************************************************************
 功能描述: 重新加载模块。mem log移动到文件里，停止记录日志，再初始化。pre_moduleName和moduleName可以相同。

     参数: pre_moduleName ，in reload前的用户模块名
           param ，in 参数值

   返回值: SDSLOG_SUCCESS    表示成功，其他表示失败
**************************************************************************** */
int SdslogReloadParam(const std::string &preModuleName, SdslogParam &param);


#endif
