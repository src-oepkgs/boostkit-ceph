/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 * Description: Define the data structure.
 */


#ifndef SDS_LOG_PARAM_H
#define SDS_LOG_PARAM_H

#include <climits>
#include <memory>
#include <string>
#include <algorithm>
#include <iostream>

static const std::string DEFAULT_USR_MODULE = "sdslog";
static const std::string DEFAULT_FULL_PATH = "/var/log/";
static const std::string DEFAULT_FULL_FILE_NAME = "/var/log/sdslog.log";
static const int DEFAULT_FILE_LOG_LEVEL = 0;
static const int DEFAULT_MEM_LOG_LEVEL = 1;
static const int DEFAULT_MEM_LOG_SIZE = 100;
static const int MIN_MEM_LOG_SIZE = 100;
static const int MAX_MEM_LOG_SIZE = 1000;

// 定义返回值
enum RetCode {
    SDSLOG_FATAL_ERROR = -99,
    SDSLOG_FAILED = -10,
    SDSLOG_CMD_FAILED = -1,
    SDSLOG_SUCCESS = 0,
    SDSLOG_INVALID_MODULE,
    SDSLOG_EMPTY_PARAM,
    SDSLOG_INVALID_PARAM,
    SDSLOG_FAIL_TO_SET_PARAM,
    SDSLOG_PARAM_FILE_CANNNOT_ACCESS,
    SDSLOG_TOO_MANY_USR_MODULES,
    SDSLOG_LOG_FILE_NOT_EXIST,
    SDSLOG_WRITE_LOG_FILE_FAIL,
    SDSLOG_LOG_FILE_BAD_ATTR,
    SDSLOG_TEMP_TOO_LARGE
};
enum LogLevel {
    LV_CRITICAL = 0,
    LV_ERROR = 1,
    LV_WARNING = 2,
    LV_INFORMATION = 3,
    LV_DEBUG = 4
};

// 参数结构体
class SdslogParam {
public:
    SdslogParam()
        : usrModuleName(DEFAULT_USR_MODULE),
          fullPath(DEFAULT_FULL_PATH),
          fullFileName(DEFAULT_FULL_FILE_NAME),
          fileLogLevel(DEFAULT_FILE_LOG_LEVEL),
          memLogLevel(DEFAULT_MEM_LOG_LEVEL),
          memLogSize(DEFAULT_MEM_LOG_SIZE)
    {}
    ~SdslogParam() {}
    void SetDefault()
    {
        usrModuleName = DEFAULT_USR_MODULE;
        fullPath = DEFAULT_FULL_PATH;
        fullFileName = DEFAULT_FULL_FILE_NAME;
        fileLogLevel = DEFAULT_FILE_LOG_LEVEL;
        memLogLevel = DEFAULT_MEM_LOG_LEVEL;
        memLogSize = DEFAULT_MEM_LOG_SIZE;
    }
    int ParseToInt(const std::string &level)
    {
        std::string lv = level;
        int ret = LV_CRITICAL;
        std::transform(lv.begin(), lv.end(), lv.begin(), ::toupper);
        if (lv == "DEBUG") {
            ret = LV_DEBUG;
        } else if (lv == "INFORMATION") {
            ret = LV_INFORMATION;
        } else if (lv == "WARNING") {
            ret = LV_WARNING;
        } else if (lv == "ERROR") {
            ret = LV_ERROR;
        } else if (lv == "CRITICAL") {
            ret = LV_CRITICAL;
        } else {
            std::cerr << "KPS_EC log level is wrong." << std::endl;
        }
        return ret;
    }
    std::string usrModuleName;
    std::string fullPath;
    std::string fullFileName;
    int fileLogLevel;
    int memLogLevel;
    unsigned int memLogSize;
};
#endif
