/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024-2024. All rights reserved.
 */
#ifndef KSAL_LIBC_H
#define KSAL_LIBC_H

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

void *memcpy(void *dest, const void *src, size_t n);

#ifdef __cplusplus
}
#endif

#endif