/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2020-2020. All rights reserved.
 *
 *
 *
 * @file    kps_compaction.h
 * @brief   压紧特性批量读写IO聚合、计数位图、chunk引用计数、Bitmap空间释放、多IO路径功能模块
 *
 * @author
 * @version  v1.0
 * @see
 * @since    2020/11/20
 */

#ifndef KPS_COMPACTION_H
#define KPS_COMPACTION_H
#include <cstring>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <fstream>
#include <string>
#include <sstream>
#include <ctime>
#include <vector>

template <typename T> constexpr inline T KpsP2align(T value, T alignValue)
{
    return value & -alignValue;
}

template <typename T> constexpr inline T KpsP2roundup(T value, T alignValue)
{
    return -(-value & -alignValue);
}

#ifdef __cplusplus
extern "C" {
#endif

#if !defined(__KERNEL__)
#include <cstdint>
#else
typedef int int32_t;
typedef unsigned long long uint64_t;
typedef long long int64_t;
#endif

#ifdef MIN
#undef MIN
#endif
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#define IMIN(a, b) \
    MIN(a, b)

#ifndef UNREFERENCE_PARAM
#define UNREFERENCE_PARAM(x) ((void)(x))
#endif

struct TagReadUsage {
    bool isAio;
    bool isSplit;
    bool isContinuous;
    uint64_t chunkSize;
    uint64_t blobReadLength;
    uint64_t pextentOffset;
    uint64_t pextentLength;
};

struct TagBlockUsage {
    uint64_t currentOffset;
    uint64_t baseOffset;
    uint64_t endPos;
    int64_t smallLength;
    int64_t leftLength;
};

struct TagBlockRelease {
    uint64_t baseOffset;
    uint64_t releaseOffset;
    uint64_t currentPos;
    uint64_t endPos;
    uint64_t chunkSize;
    uint64_t forwardStep;
    int64_t smallLength;
    int64_t leftLength;
};

void CompactionReadInit(uint64_t gol,
        uint64_t feo,
        uint64_t gcs,
        bool aac,
        TagReadUsage &readParam);

void CompactionReadRegion(uint64_t pextentOffset,
                          uint64_t pextentLength,
                          uint64_t &blobReadLength,
                          uint64_t chunkSize,
                          bool isCompactionSwitchOpen);

void CompactionBlockUsageInit(uint64_t pextentOffset,
                              uint64_t pextentLength,
                              uint64_t minAllocSize,
                              TagBlockUsage &param);

void CompactionBlockUsageInternal(uint64_t mas,
                                  uint64_t po,
                                  TagBlockUsage &param);

void CompactionBlockReleaseInit(uint64_t minAllocSize,
                                uint64_t pextentOffset,
                                uint64_t pextentLength,
                                TagBlockRelease &releaseParam);

void CompactionBlockReleaseInternal(uint64_t pextentOffset,
                                    TagBlockRelease &releaseParam);

int64_t ReuseCountClose(std::vector<std::int8_t> &count_number);
									
#ifdef __cplusplus
}
#endif
#endif /* _KPS_COMPACTION_H */
