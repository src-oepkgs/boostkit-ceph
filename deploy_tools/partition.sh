#!/bin/bash

parted -s /dev/nvme0n1 mklabel gpt

start=4
for i in {1..6}
do
end=`expr $start + 10240`
parted /dev/nvme0n1 mkpart primary ${start}MiB ${end}MiB
start=$end

end=`expr $start + 25600`
parted /dev/nvme0n1 mkpart primary ${start}MiB ${end}MiB
start=$end
done
parted /dev/nvme0n1 mkpart primary ${end}MiB 100%
